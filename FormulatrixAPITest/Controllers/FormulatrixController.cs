﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FormulatrixAPITest.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FormulatrixAPITest.Controllers
{
    /// <summary>
    /// Repository XML and JSON string
    /// </summary>
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class FormulatrixController : ControllerBase
    {

        private readonly IFormulatrixService _service;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public FormulatrixController(IFormulatrixService service)
        {
            _service = service;
        }

        /// <summary>
        /// Store an item to the repository.
        /// Parameter itemType is used to differentiate JSON or XML.
        /// </summary>
        /// <param name="itemName">key name to search and delete item in repository</param>
        /// <param name="itemContent">XML or JSON string</param>
        /// <param name="itemType">1 = itemContent is a JSON string. 2 = itemContent is an XML string.</param>
        /// <returns>OK Success when validation success, Bad Request if validation fail</returns>
        [HttpGet]
        public ActionResult Register(string itemName, string itemContent, int itemType)
        {
            string result = _service.Register(itemName, itemContent, itemType);
            if (result != "success")
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        /// <summary>
        /// Retrieve an item from the repository
        /// </summary>
        /// <param name="itemName">Key name from when you registration the Item Content</param>
        /// <returns>string Item Content, NOT FOUND if key name doesnt exist on repository</returns>
        [HttpGet]
        public ActionResult Retrieve(string itemName)
        {
            string result = _service.Retrieve(itemName);

            if (result != "bad")
            {
                return Ok(result);
            }

            return BadRequest("itemName doesn't exist on repository");
        }

        /// <summary>
        /// Retrieve the type of the item (JSON or XML).
        /// </summary>
        /// <param name="itemName">Key name from when you registration the Item Content</param>
        /// <returns>1 = itemContent is a JSON string. 2 = itemContent is an XML string. , NOT FOUND if key name doesnt exist on repository</returns>
        [HttpGet]
        public ActionResult GetType(string itemName)
        {
            int result = _service.GetType(itemName);
            if (result == -1)
            {
                return BadRequest("itemName doesn't exist on repository");
            }
            return Ok(result);
        }

        /// <summary>
        /// Remove an item from the repository.
        /// </summary>
        /// <param name="itemName">Key name from when you registration the Item Content</param>
        /// <returns>OK Success when item is exist on repository and successfull to be deleted, Bad Request when item is doesnt exist on repository</returns>
        [HttpDelete]
        public ActionResult Deregister(string itemName)
        {
            string result = _service.Deregister(itemName);
            if (result != "bad")
            {
                return Ok(result);
            }

            return BadRequest("itemName doesn't exist on repository");
        }
    }
}

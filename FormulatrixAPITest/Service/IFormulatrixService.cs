﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FormulatrixAPITest.Service
{
    public interface IFormulatrixService
    {
        string Register(string itemName, string itemContent, int itemType);
        string Retrieve(string itemName);
        int GetType(string itemName);
        string Deregister(string itemName);
    }
}

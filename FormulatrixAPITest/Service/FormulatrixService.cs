﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace FormulatrixAPITest.Service
{
    public class FormulatrixService : IFormulatrixService
    {
        public Dictionary<string, string> stringRepository;

        public FormulatrixService()
        {
            stringRepository = new Dictionary<string, string>();
        }  

        public string Register(string itemName, string itemContent, int itemType)
        {
            if (stringRepository.ContainsKey(itemName))
            {
                return "itemName is exist on repository, please use another itemName or deregister it first";
            }

            var checkType = JSONorXML(itemContent);
            if (checkType == itemType)
            {
                stringRepository.Add(itemName, itemContent);
            }
            else
            {
                return "itemContent format (JSON or XML) and itemType (1 or 2) is not match, please check your input again";
            }
            
            return "success";
        }

        public string Retrieve(string itemName)
        {
            if (stringRepository.ContainsKey(itemName))
            {
                var result = stringRepository[itemName];
                return result;
            }
            else
            {
                return "bad";
            }

        }
        public int GetType(string itemName)
        {
            if (stringRepository.ContainsKey(itemName))
            {
                var isi = stringRepository[itemName];
                return JSONorXML(isi);
            }
            else
            {
                return -1;
            }
        }

        public string Deregister(string itemName)
        {
            if (stringRepository.ContainsKey(itemName))
            {
                _ = stringRepository.Remove(itemName);
                return "success";
            }
            else
            {
                return "bad";
            }
        }

        public int JSONorXML(string itemContent)
        {
            var result = 0;

            try
            {
                var test = JsonSerializer.Deserialize<Dictionary<string, object>>(itemContent);
                result = 1;
            }
            catch (Exception e)
            {
                try
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(itemContent.Trim());
                    result = 2;
                }
                catch (Exception w)
                {
                }//try catch 2
            }//try catch 1

            return result;
        }
    }
}
